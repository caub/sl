# Notes manager

### Run it as a TCP server

Start server: `PORT=1337 node src/index`

Examples of commands:

```bash
nc localhost 1337 <<EOF
create:doca
insert:doca:0:Hello
format:doca:0:5:bold
insert:doca:5: big
insert:doca: World!
format:doca:10:17:link::https://en.wikipedia.org/wiki/World
format:doca:10::inlineCode
get:doca:md
delete:doca
EOF
```

Outputs:
```
200
200
200
200
200
200
**Hello** big [`World!`](https://en.wikipedia.org/wiki/World)
200
```

### CLI usage
```bash
node src/cli <<EOF
create:x
insert:x:Foo bar qux lol wut!
format:x:4:11:link::https://yes.no
format:x:4:7:bold
format:x:8:11:inlineCode
format:x:6:9:italic
get:x:md
EOF
```

### messageHandler (process individual commands)
```js
const handleMessage = require('./messageHandler')
const output1 = handleMessage('create:x')
const output2 = handleMessage('insert:x:Hello peeps')
```