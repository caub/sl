const Note = require('./Note');
const util = require('util');

// in-memory storage for now, we could use redis, pg, ..

const notesMap = new Map(); // Map noteId => Node

const status = value => `${value}\r\n`;

const handleMessage = msg => {
  const parts = msg.split(':');
  if (!handlers[parts[0]]) return status(405);
  return handlers[parts[0]](...parts.slice(1));
};

module.exports = handleMessage;


const handlers = {
  help: () => `Examples of commands:
> create:doca
> insert:doca:0:Hello
> format:doca:0:5:bold
> insert:doca:5: big\n
> insert:doca: World!\n
> get:doca:txt
> get:doca:md
> delete:doca
`,
  create: id => {
    if (!id) return status(422);
    // if (notesMap.has(id)) return status(409); // overwriting is allowed
    notesMap.set(id, new Note());
    return status(200); // 204 would be more appropriate
  },
  insert: (id, ...strs) => { // String args
    const [position, text] = strs.length > 1 && /^\d+$/.test(strs[0]) ?
      [+strs[0], strs.slice(1).join(':')] :
      [Infinity, strs.join(':')];
    if (!text) return status(422);

    const note = notesMap.get(id);
    if (!note) return status(404);
    note.insert(position, text);
    return status(200);
  },
  delete: id => {
    if (!notesMap.has(id)) return status(404);
    notesMap.delete(id);
    return status(200);
  },
  get: (id, format = 'txt', arg) => {
    const note = notesMap.get(id);
    if (!note) return status(404);

    if (format === 'txt') {
      return note.root.toText() + '\r\n';
    }
    if (format === 'md') {
      return note.root.toMd() + '\r\n';
    }
    if (process.env.NODE_ENV !== 'production' && format === '_debug') {
      return util.inspect(note.root, { depth: arg || 7 });
    }

    return status(422); // status responses can conflict with above notes content responses
  },
  // normalize: (id) => {
  //   const note = notesMap.get(id);
  //   if (!note) return status(404);
  //   note.root.normalize(true);
  //   return status(200)
  // },
  format: (id, positionStart, positionEnd, _type, ...rest) => { // String args
    const posStart = +positionStart;
    let posEnd = positionEnd === '' ? Infinity : +positionEnd;
    if (posStart && posEnd && posStart >= posEnd) return status(204);

    const note = notesMap.get(id);
    if (!note) return status(404);

    const type = _type === 'i' ? 'italic' : _type === 'b' ? 'bold' : _type; // some shortcuts for italic/bold
    let attrs = {};
    if (type === 'link' && rest.length > 1) {
      attrs = {
        embed: rest[0],
        url: rest.slice(1).join(':'), // our argument separator is :, since links contain :, join with it
      };
    }
    else if (type === 'code' && rest.length > 0) {
      attrs = {
        lang: rest[0]
      };
    }

    note.format(posStart, posEnd, type, attrs);

    return status(200);
  },
};

