/*
enum NodeType { bold, italic, link, inlineCode, code }
interface Node {
  type: NodeType
  text?: String
  children?: Array<Node> // when children is not empty, text is ignored
  parent?: Node,
  attrs: Object
}
*/

const mdFormatters = {
  italic: s => `*${s}*`, // italic text
  bold: s => `**${s}**`, // bold text
  link: (s, { url, embed }) => `${embed ? '!' : ''}[${s}](${url})`, // Links markdown formats
  inlineCode: s => `\`${s}\``,
  code: (s, { lang = '' }) => `\n\`\`\`${lang}\n${s}\n\`\`\`\n`,
};

class Node {
  constructor({ type, text, children = [], parent, attrs = {} } = {}) {
    this.type = type; // can be undefined, like a basic text Node, with text, or a container node (with children)
    this.text = text; // A node has either non-empty children or non-empty text, not both at same time
    this.parent = parent;
    this.attrs = { ...attrs }; // attributes, like {url: 'http://foo.bar', embed: false}
    this.append(...children);
  }
  append(...nodes) {
    const children = (this.children || []).filter(n => !nodes.includes(n));
    for (const node of nodes) {
      node.parent = this;
    }
    this.children = [...children, ...nodes];
    return this;
  }
  prepend(...nodes) {
    const children = (this.children || []).filter(n => !nodes.includes(n));
    for (const node of nodes) {
      node.parent = this;
    }
    this.children = [...nodes, ...children];
    return this;
  }

  replaceWith(...nodes) {
    const { parent } = this;
    const i = parent.children.indexOf(this);
    if (i === -1) throw new RangeError('child-parent relation not respected');
    for (const node of nodes) {
      node.parent = parent;
    }
    // TODO: throw if one of nodes contains parent
    parent.children = [...parent.children.slice(0, i), ...nodes, ...parent.children.slice(i + 1)];
  }
  closest(type) {
    let node = this;
    while (node) {
      if (node.type === type) return node;
      node = node.parent;
    }
  }
  // furthest(type) {
  //   let node = this;
  //   let furthestMatch;
  //   while (node) {
  //     if (node.type === type) furthestMatch = node;
  //     node = node.parent;
  //   }
  //   return furthestMatch;
  // }

  toText() {
    return this.children.length ? this.children.map(c => c.toText()).join('') : this.text || '';
  }
  toMd() {
    const mdFormatter = mdFormatters[this.type] || (s => s);
    const mdContent = this.children.length ? this.children.map(c => c.toMd()).join('') : this.text || '';
    return mdContent ? mdFormatter(mdContent, this.attrs) : '';
  }
  toMarkdown() {
    return this.toMd();
  }
  positionStart() { // returns the "cursor" position at the start of this node // we could cache it in the nodes returned in findNodesBetween
    if (!this.parent) return 0;
    const siblings = this.parent.children;
    let offsetPosFromParent = 0;
    for (let n of siblings) {
      if (n === this) break;
      offsetPosFromParent += n.toText().length;
    }
    return this.parent.positionStart() + offsetPosFromParent;
  }
  positionEnd() {
    return this.positionStart() + this.toText().length;
  }

  normalize(recursive) { // filter out empty children nodes, and join together normal|italic|bold|inlineCode consecutive children nodes (else 2 consecutive italic looks like a bold start).
    let len, newChildren;
    do {
      len = this.children.length;
      newChildren = [];
      for (const c of this.children) {
        const last = newChildren[newChildren.length - 1];
        // if last and c are 'similar' (same type, and attrs if type is link)
        if (last && last.type === c.type && (!c.type || c.type === 'bold' || c.type === 'italic' || c.type === 'inlineCode' || c.type === 'link' && c.attrs.embed === last.attrs.embed && c.attrs.url === c.attrs.url)) {
          if (!last.children.length && !c.children.length) { // both c and last are text elements
            last.text += c.text;
          } else if (last.children.length && c.children.length) { // both c and last have children
            last.append(c.children);
          } else if (last.children.length) { // if last has children and c is a text
            last.append(new Node({ ...c, type: undefined, attrs: {} }));
          } else { // if last is a text and c has children
            // gonna replace last by c with last prepended in it
            newChildren[newChildren.length - 1] = c;
            c.prepend(new Node({ ...last, type: undefined, attrs: {} }));
          }
        } else if (c.text || c.children.length) { // filter out empty nodes
          newChildren.push(c);
        }
      }
      this.children = newChildren;
    } while (len !== newChildren.length); // continue to optimize as long as it decreases

    // unwrap 'useless' container child nodes
    for (const child of this.children) {
      if (child.children.length && !child.type) {
        child.replaceWith(...child.children);
      }
    }

    if (recursive) {
      for (const child of this.children) {
        child.normalize(recursive);
      }
    }
  }

  /**
   * search for the leaf nodes located (fully or partly) between posStart and posEnd text positions
   * @returns Array<Node>
   * @param {*} posStart
   * @param {*} posEnd
   * @param {*} offset cumulated text offset length up the tree
   */
  findNodesBetween(posStart, posEnd, offset = 0) {
    if (!this.children.length) {
      const txtLen = (this.text || '').length;
      return (posStart < offset + txtLen && posEnd > offset) ? [this] : [];
    }
    let parentOffset = 0;
    const nodes = [];
    for (const c of this.children) {
      nodes.push(...c.findNodesBetween(posStart, posEnd, offset + parentOffset))
      parentOffset += c.toText().length;
      // we could optimize this with a break; as soon as we found nodes, and then c.findNodesBetween is empty again (reached the end)
      // todo try with: if (!c.children.length && (offset + parentOffset >= posEnd)) break;
    }
    return nodes;
  }

}

module.exports = Node;
