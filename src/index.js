const net = require('net');
const handleMessage = require('./messageHandler');
const splitLines = require('./splitLines');

const server = net.createServer(c => {
  // 'connection' listener
  // client connected
  // we could use for await (asyncIterator) as well
  let cur = ''; // keep the current incomplete message line
  c.on('data', d => {
    const txt = cur + d; // prepend cur (and it also convert c Buffer to text)
    const lines = splitLines(txt);
    const processableLines = lines.slice(0, -1);
    cur = lines[lines.length - 1];
    for (const line of processableLines) {
      try {
        c.write(handleMessage(line));
      } catch (e) {
        console.error(e);
        c.write(e.message);
      }
    }
  })
  // c.on('end', () => {
  //   // client disconnected, we could remove c from a list of active clients if we did broadcasting/push
  // });
});
server.on('error', err => {
  throw err
});

const start = (port = process.env.PORT || '1337') => server.listen(port);

module.exports = start;

if (!module.parent) {
  start();
  console.log('server started on port', process.env.PORT || '1337');
}
