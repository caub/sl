const Node = require('./Node');

// from a nodes array of leaf nodes, get a covering path of those nodes (for example if nodes contains the whole children of a node, replace them by the parent) // this allows to apply a given type to less nodes in format()
function getCommonAncestorsNodes(nodes) {
  const newNodes = [];
  let changed = false;
  for (let i = 0; i < nodes.length; i++) {
    const cs = nodes[i].parent.children;
    const last = cs[cs.length - 1];
    if (cs.indexOf(nodes[i]) === 0 && nodes.indexOf(last) >= 0) { // we got a whole set of children of a node
      newNodes.push(nodes[i].parent);
      i += nodes.indexOf(last);
      changed = true;
    } else {
      newNodes.push(nodes[i]);
    }
  }
  if (!changed) return newNodes;
  return getCommonAncestorsNodes(newNodes); // try to simplify more
}

// todo: a global normalize to remove useless nodes like a parent with 1 child and no type

class Note {
  constructor() {
    this.root = new Node();
  }

  insert(position, text) { // mutates the tree
    const [nodeAtPosition] = this.root.findNodesBetween(position, position + 1);

    if (!nodeAtPosition) { // insert at the end
      this.root.append(new Node({ text }));
    } else {
      const nodeStartPos = nodeAtPosition.positionStart();
      nodeAtPosition.replaceWith(
        new Node({ ...nodeAtPosition, text: nodeAtPosition.text.slice(0, position - nodeStartPos) }),
        new Node({ text }),
        new Node({ ...nodeAtPosition, text: nodeAtPosition.text.slice(position - nodeStartPos) }),
      );
      nodeAtPosition.parent.normalize();
    }
  }

  format(posStart, posEnd, type, attrs = {}) { // mutates the tree
    const nodes = this.root.findNodesBetween(posStart, posEnd);

    if (!nodes.length) return; // nothing to do!

    /* TODO code, inlineCode not handled for now, it shouldn't be hard
      - code can't be nested nor accept children (so just give it text: this.root.toText().slice(postStart, posEnd))
      - inlineCode can be nested, but doesn't have children (so just give it text: this.root.toText().slice(postStart, posEnd))
      note: we could also just handle that in toText/toMd
    */

    const hasType = nodes.some(node => node.closest(type));
    // if hasType, we'll toggle type (aka remove all type in the nodes range), else we'll add type to nodes

    const firstNodePosStart = nodes[0].positionStart();
    const lastNodePosStart = nodes[nodes.length - 1].positionStart();

    // handle first node 
    if (hasType) {
      const cFirst = nodes[0].closest(type);
      if (cFirst === nodes[0]) {
        nodes[0].replaceWith(
          new Node({ ...nodes[0], text: nodes[0].text.slice(0, posStart - firstNodePosStart) }),
          new Node({ ...nodes[0], type: undefined, attrs: {}, text: nodes[0].text.slice(posStart - firstNodePosStart, posEnd - firstNodePosStart) }),
          new Node({ ...nodes[0], text: nodes[0].text.slice(posEnd - firstNodePosStart) }), // will be empty if nodes.length > 1, and empty nodes get removed by normalize()
        );
      } else if (cFirst) {
        const cFirstPosStart = cFirst.positionStart();
        cFirst.replaceWith(...cFirst.children); // remove cStart, and put its children instead
        // we must apply back type to the start part of cFirst, since we dropped the node with type
        this.format(cFirstPosStart, posStart, type, attrs); // apply type to the left part of cFirst
      }
      // else nothing to do
    } else {
      nodes[0].append(
        new Node({ ...nodes[0], type: undefined, attrs: {}, text: nodes[0].text.slice(0, posStart - firstNodePosStart) }),
        new Node({ ...nodes[0], type, attrs, text: nodes[0].text.slice(posStart - firstNodePosStart, posEnd - firstNodePosStart) }),
        new Node({ ...nodes[0], type: undefined, attrs: {}, text: nodes[0].text.slice(posEnd - firstNodePosStart) }),
      );
    }

    // handle last node if not the same than first
    if (nodes.length > 1) {
      if (hasType) {
        const cLast = nodes[nodes.length - 1].closest(type);
        if (cLast === nodes[nodes.length - 1]) {
          nodes[nodes.length - 1].replaceWith(
            new Node({ ...nodes[nodes.length - 1], type: undefined, attrs: {}, text: nodes[nodes.length - 1].text.slice(0, posEnd - lastNodePosStart) }),
            new Node({ ...nodes[nodes.length - 1], text: nodes[nodes.length - 1].text.slice(posEnd - lastNodePosStart) }),
          );
        } else if (cLast) {
          const cLastPosEnd = cLast.positionEnd();
          cLast.replaceWith(...cLast.children); // remove cStart, and put its children instead
          // we must apply back type to the end part of cLast, since we dropped the node with type
          this.format(posEnd, cLastPosEnd, type, attrs); // apply type to the right part of cLast
        }
        // else nothing to do
      } else {
        nodes[nodes.length - 1].append(
          new Node({ ...nodes[nodes.length - 1], type, attrs, text: nodes[nodes.length - 1].text.slice(0, posEnd - lastNodePosStart) }),
          new Node({ ...nodes[nodes.length - 1], type: undefined, attrs: {}, text: nodes[nodes.length - 1].text.slice(posEnd - lastNodePosStart) }),
        );
      }

    }

    // handle the rest (nodes[1:-1])
    if (hasType) { // remove type nodes
      for (let i = 1; i < nodes.length - 1; i++) {
        const c = nodes[i].closest(type);
        if (c) {
          c.replaceWith(...c.children);
        }
      }
    } else {
      const compactNodes = getCommonAncestorsNodes(nodes.slice(1, -1)); // 'compact' them
      for (const node of compactNodes) {
        const newNode = new Node({ type, attrs });
        node.replaceWith(newNode);
        newNode.append(node);
      }
    }

    this.root.normalize(true);
  }
}

module.exports = Note;
