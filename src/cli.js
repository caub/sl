const handleMessage = require('./messageHandler');
const splitLines = require('./splitLines');

const handleInput = s => {
  const cmds = splitLines(s).filter(l => l);
  let output;
  for (const cmd of cmds) {
    output = handleMessage(cmd);
  }
  return output; // return last output
}

module.exports = handleInput;

if (!module.parent) {
  (async () => {
    let s = '';
    // for await (const c of process.stdin) s += c; // it makes jest confused, and too much work to add babel right now
    process.stdin.on('data', c => { s += c; });
    process.stdin.on('end', () => console.log(handleInput(s)));
  })();
}
