// split a String by lines, but preserve trailing lines, and ending new lines too
// split('foo\n\nbar\nwut') -> ['foo\n', 'bar', 'wut']
// split('foo\n\nbar\nwut\n') -> ['foo\n', 'bar', 'wut', '']
const splitLines = (str = '') => {
  const lines = str.split(/\n/);
  if (lines.length <= 1) return lines;
  return lines.slice(1, -1).reduce((a, s) => s === '' ? [...a.slice(0, -1), a[a.length - 1] + '\n'] : [...a, s], [lines[0]]).concat(lines[lines.length - 1]); // not very beautiful and fast, can be improved if needed
}

module.exports = splitLines;
