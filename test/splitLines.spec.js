const splitLines = require('../src/splitLines');

describe('splitLines', () => {
  it('splits', () => {
    expect(splitLines('foo\n\nbar\nwut')).toEqual(['foo\n', 'bar', 'wut']);
    expect(splitLines('foo\n\nbar\nwut\n')).toEqual(['foo\n', 'bar', 'wut', '']);
  });
});
