const cli = require('../src/cli');

// tests of combined commands, like a stress test

describe('multi-format', () => {
  it('handle correctly nested nodes and toggling', async () => {
    const output = cli(`create:x
insert:x:Foo bar qux lol wut!
format:x:4:11:link::https://yes.no
format:x:4:7:bold
format:x:8:11:inlineCode
format:x:6:9:italic
format:x:6:8:bold
get:x:md
`)
    expect(output).toEqual(`Foo [**ba***r *\`*q*ux\`](https://yes.no) lol wut!\r\n`)
  })
})
