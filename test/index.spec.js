const net = require('net');
const runNotesServer = require('../src/index');

const HOST = 'localhost'
const PORT = 2337

function generateDocId() {
  return `doc${Math.floor(Math.random() * 1000)}`
}

function runCommand(command) {
  return new Promise((resolve) => {
    const client = new net.Socket()
    client.connect(PORT, HOST, () => {
      client.on('data', (data) => {
        resolve(data.toString('utf8'))
        client.end()
      })
      client.write(command + '\n')
    })
  })
}

let server;
beforeAll(() => {
  server = runNotesServer(PORT);
});

afterAll(() => {
  server.close();
})

describe('Slite 1986 Test', () => {
  describe('create', () => {
    it('creates a doc with a 200 response', async () => {
      const docId = generateDocId()
      const resp = await runCommand(`create:${docId}`)
      expect(resp).toEqual('200\r\n')
    })

    it('creates a doc by storing an empty document', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      const resp = await runCommand(`get:${docId}:txt`)
      expect(resp).toEqual('\r\n')
    })

    it('creates a doc by overriding an existing one', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:hello`)
      await runCommand(`create:${docId}`)
      const resp = await runCommand(`get:${docId}:txt`)
      expect(resp).toEqual('\r\n')
    })
  })

  describe('insert', () => {
    it('returns a 404 if doc has not been created', async () => {
      const docId = generateDocId()
      await runCommand(`insert:${docId}:Hello`)
      const resp = await runCommand(`get:${docId}:txt`)
      expect(resp).toEqual('404\r\n')
    })

    it('inserts content at first position', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:0:Hello`)
      const resp = await runCommand(`get:${docId}:txt`)
      expect(resp).toEqual('Hello\r\n')
    })

    it('inserts at multiple position', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello`)
      await runCommand(`insert:${docId}:5:World`)
      await runCommand(`insert:${docId}:5: `)
      await runCommand(`insert:${docId}:11:!`)
      const resp = await runCommand(`get:${docId}:txt`)
      expect(resp).toEqual('Hello World!\r\n')
    })

    it('inserts break line', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello\n`)
      await runCommand(`insert:${docId}:World!\n`)
      const resp = await runCommand(`get:${docId}:txt`)
      expect(resp).toEqual('Hello\nWorld!\n\r\n')
    })
  })

  describe('formats', () => {
    it('formats a document in bold', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello World!\n`)
      await runCommand(`format:${docId}:0:5:bold`)
      const resp = await runCommand(`get:${docId}:md`)
      expect(resp).toEqual('**Hello** World!\n\r\n')
    })

    it('formats a document in italic', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello World!\n`)
      await runCommand(`format:${docId}:0:5:italic`)
      const resp = await runCommand(`get:${docId}:md`)
      expect(resp).toEqual('*Hello* World!\n\r\n')
    })

    it('formats a document with a link', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello World!`)
      await runCommand(`format:${docId}:6:11:link::https://google.com`)
      const resp = await runCommand(`get:${docId}:md`)
      expect(resp).toEqual('Hello [World](https://google.com)!\r\n')
    })

    //     it('formats a document with inline code', async () => {
    //       const docId = generateDocId()
    //       await runCommand(`create:${docId}`)
    //       await runCommand(`insert:${docId}:Hello World!`)
    //       await runCommand(`format:${docId}:6:11:inlineCode`)
    //       const resp = await runCommand(`get:${docId}:md`)
    //       expect(resp).toEqual('Hello `World`!\r\n')
    //     })

    //     it('formats a document with code', async () => {
    //       const docId = generateDocId()
    //       await runCommand(`create:${docId}`)
    //       await runCommand(`insert:${docId}:Hello 'World' + '!'`)
    //       await runCommand(`format:${docId}:6:19:code:js`)
    //       const resp = await runCommand(`get:${docId}:md`)
    //       expect(resp).toEqual(`Hello 
    // \`\`\`js
    // 'World' + '!'
    // \`\`\`
    // \r\n`)
    //     })

    it('formats at multiple position', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello`)
      await runCommand(`insert:${docId}:5:World`)
      await runCommand(`insert:${docId}:5: `)
      await runCommand(`format:${docId}:5:7:italic`)
      await runCommand(`format:${docId}:9:11:bold`)
      const resp = await runCommand(`get:${docId}:md`)
      expect(resp).toEqual('Hello* W*or**ld**\r\n')
    })

    it('formats a document while maintaining text intact', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello`)
      await runCommand(`format:${docId}:0:5:italic`)
      await runCommand(`insert:${docId}:5: World!`)

      const respmd = await runCommand(`get:${docId}:md`)
      expect(respmd).toEqual('*Hello* World!\r\n') // removed a trailing \n there

      const resptxt = await runCommand(`get:${docId}:txt`)
      expect(resptxt).toEqual('Hello World!\r\n')
    })

    it('formats a document until the end if positionEnd is larger than the total length', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello World!`)
      await runCommand(`format:${docId}:6:9000:link::https://google.com`)
      const resp = await runCommand(`get:${docId}:md`)
      expect(resp).toEqual('Hello [World!](https://google.com)\r\n')
    })

    it('formats a document until the end if positionEnd is left empty', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello World!`)
      await runCommand(`format:${docId}:6::link::https://google.com`)
      const resp = await runCommand(`get:${docId}:md`)
      expect(resp).toEqual('Hello [World!](https://google.com)\r\n')
    })

    it('formats with nested types!', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello World`)
      await runCommand(`format:${docId}:5:9:italic`)
      await runCommand(`format:${docId}:6:8:bold`)
      const resp = await runCommand(`get:${docId}:md`)
      expect(resp).toEqual('Hello* **Wo**r*ld\r\n')
    });

    it('toggles formats', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`insert:${docId}:Hello World`)
      await runCommand(`format:${docId}:5:9:italic`)
      await runCommand(`format:${docId}:3:7:italic`)
      const resp = await runCommand(`get:${docId}:md`)
      expect(resp).toEqual('Hello W*or*ld\r\n')
    });
  })

  describe('delete', () => {
    it('deletes a document', async () => {
      const docId = generateDocId()
      await runCommand(`create:${docId}`)
      await runCommand(`delete:${docId}`)
      const resp = await runCommand(`get:${docId}:txt`)
      expect(resp).toEqual('404\r\n')
    })
  })
})
